-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Lis 2018, 13:10
-- Wersja serwera: 10.1.28-MariaDB
-- Wersja PHP: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szkola_iia`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `godziny`
--

DROP TABLE IF EXISTS `godziny`;
CREATE TABLE `godziny` (
  `id_godzina` bigint(20) UNSIGNED NOT NULL,
  `godzina_rozpoczecia` time NOT NULL,
  `dzien` tinyint(1) UNSIGNED NOT NULL,
  `id_przedmiot` bigint(20) UNSIGNED NOT NULL,
  `czas_trwania` tinyint(3) UNSIGNED NOT NULL COMMENT 'Podawane w minutach',
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL,
  `id_sala` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `godziny_plany`
--

DROP TABLE IF EXISTS `godziny_plany`;
CREATE TABLE `godziny_plany` (
  `id_godzina` bigint(20) UNSIGNED NOT NULL,
  `id_plan` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy`
--

DROP TABLE IF EXISTS `klasy`;
CREATE TABLE `klasy` (
  `id_klasa` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(10) NOT NULL,
  `data_utworzenia` date NOT NULL,
  `id_kierunek` bigint(20) UNSIGNED NOT NULL,
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL COMMENT 'Nauczyciel opiekun/wychowawca'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klasy_uczniowie`
--

DROP TABLE IF EXISTS `klasy_uczniowie`;
CREATE TABLE `klasy_uczniowie` (
  `id_uczen` bigint(20) UNSIGNED NOT NULL,
  `id_klasa` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kody_pocztowe`
--

DROP TABLE IF EXISTS `kody_pocztowe`;
CREATE TABLE `kody_pocztowe` (
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `kod` char(6) NOT NULL,
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kody_pocztowe`
--

INSERT INTO `kody_pocztowe` (`id_kod_pocztowy`, `kod`, `id_miejscowosc`) VALUES
(1, '42-200', 1),
(2, '42-212', 1),
(3, '42-220', 1),
(4, '42-226', 1),
(5, '42-134', 1),
(6, '42-229', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `miejscowosci`
--

DROP TABLE IF EXISTS `miejscowosci`;
CREATE TABLE `miejscowosci` (
  `id_miejscowosc` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `miejscowosci`
--

INSERT INTO `miejscowosci` (`id_miejscowosc`, `nazwa`) VALUES
(7, 'Bogatynia'),
(1, 'Częstochowa'),
(3, 'Gdynia'),
(4, 'Katowice'),
(5, 'Kraków'),
(6, 'Nowy Sącz'),
(2, 'Olkusz');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `nauczyciele`
--

DROP TABLE IF EXISTS `nauczyciele`;
CREATE TABLE `nauczyciele` (
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL,
  `osoba` bigint(20) UNSIGNED NOT NULL,
  `data_zatrudnienia` date NOT NULL,
  `data_zwolnienia` date NOT NULL,
  `stopien` bigint(20) UNSIGNED NOT NULL,
  `pensja` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `nauczyciele`
--

INSERT INTO `nauczyciele` (`id_nauczyciel`, `osoba`, `data_zatrudnienia`, `data_zwolnienia`, `stopien`, `pensja`) VALUES
(1, 1, '2016-11-08', '0000-00-00', 0, '2500.30'),
(2, 5, '2018-11-04', '0000-00-00', 0, '2439.78');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `osoby`
--

DROP TABLE IF EXISTS `osoby`;
CREATE TABLE `osoby` (
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `imie` varchar(25) NOT NULL,
  `nazwisko` varchar(35) NOT NULL,
  `pesel` char(11) NOT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nr_posesja` int(5) NOT NULL,
  `nr_lokal` int(5) NOT NULL,
  `telefon` char(12) NOT NULL,
  `e_mail` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `osoby`
--

INSERT INTO `osoby` (`id_osoba`, `imie`, `nazwisko`, `pesel`, `id_kod_pocztowy`, `id_ulica`, `nr_posesja`, `nr_lokal`, `telefon`, `e_mail`) VALUES
(1, 'Jadwiga', 'Biernacka', '8888888888', 1, 1, 12, 0, '888-888-888', ''),
(2, 'Kamil', 'Baton', '66666666666', 2, 4, 15, 3, '', ''),
(3, 'Andrzej', 'Brzęczyszczykiewicz', '11111111111', 5, 1, 145, 14, '', ''),
(4, 'Justyna', 'Grabowska', '00000000000', 2, 3, 90, 11, '112', 'jakis@cos.pl'),
(5, 'Cezary', 'Porażka', '44444444444', 1, 1, 13, 0, '', '');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `pensje_nauczycieli`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `pensje_nauczycieli`;
CREATE TABLE `pensje_nauczycieli` (
`imie` varchar(25)
,`nazwisko` varchar(35)
,`nazwa` varchar(60)
,`kod` char(6)
,`pensja` decimal(6,2)
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `plany_zajec`
--

DROP TABLE IF EXISTS `plany_zajec`;
CREATE TABLE `plany_zajec` (
  `id_plan` bigint(20) UNSIGNED NOT NULL,
  `id_klasa` bigint(20) UNSIGNED NOT NULL,
  `data_obowiazywania` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty`
--

DROP TABLE IF EXISTS `przedmioty`;
CREATE TABLE `przedmioty` (
  `id_przedmiot` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmioty_nauczyciele`
--

DROP TABLE IF EXISTS `przedmioty_nauczyciele`;
CREATE TABLE `przedmioty_nauczyciele` (
  `id_przedmiot` bigint(20) UNSIGNED NOT NULL,
  `id_nauczyciel` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sale`
--

DROP TABLE IF EXISTS `sale`;
CREATE TABLE `sale` (
  `id_sala` bigint(20) UNSIGNED NOT NULL,
  `id_szkola` bigint(20) UNSIGNED NOT NULL,
  `sala_numer` varchar(5) NOT NULL,
  `pietro` varchar(5) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `szkoly`
--

DROP TABLE IF EXISTS `szkoly`;
CREATE TABLE `szkoly` (
  `id_szkola` bigint(20) UNSIGNED NOT NULL,
  `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL,
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nr_posesja` varchar(5) NOT NULL,
  `nr_lokal` varchar(5) NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `patron` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie`
--

DROP TABLE IF EXISTS `uczniowie`;
CREATE TABLE `uczniowie` (
  `uczen` bigint(20) UNSIGNED NOT NULL,
  `id_osoba` bigint(20) UNSIGNED NOT NULL,
  `data_zapisu` date NOT NULL,
  `data_rozpoczecia` date NOT NULL,
  `data_zakonczenia` date NOT NULL,
  `forma_nauki` bigint(20) UNSIGNED NOT NULL,
  `kierunek` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ulice`
--

DROP TABLE IF EXISTS `ulice`;
CREATE TABLE `ulice` (
  `id_ulica` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ulice`
--

INSERT INTO `ulice` (`id_ulica`, `nazwa`) VALUES
(1, 'Fiołkowa'),
(2, 'Makowa'),
(3, 'Narcyzowa'),
(4, 'Lipowa'),
(5, 'Sosonowa'),
(6, 'Batonowa'),
(7, 'Warszawska');

-- --------------------------------------------------------

--
-- Struktura widoku `pensje_nauczycieli`
--
DROP TABLE IF EXISTS `pensje_nauczycieli`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `pensje_nauczycieli`  AS  select `osoby`.`imie` AS `imie`,`osoby`.`nazwisko` AS `nazwisko`,`miejscowosci`.`nazwa` AS `nazwa`,`kody_pocztowe`.`kod` AS `kod`,`nauczyciele`.`pensja` AS `pensja` from (((`osoby` join `miejscowosci`) join `kody_pocztowe`) join `nauczyciele`) where ((`osoby`.`id_osoba` = `nauczyciele`.`osoba`) and (`osoby`.`id_kod_pocztowy` = `kody_pocztowe`.`id_kod_pocztowy`) and (`kody_pocztowe`.`id_miejscowosc` = `miejscowosci`.`id_miejscowosc`)) ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `godziny`
--
ALTER TABLE `godziny`
  ADD UNIQUE KEY `id_godzina` (`id_godzina`),
  ADD KEY `id_przedmiot` (`id_przedmiot`),
  ADD KEY `id_nauczyciel` (`id_nauczyciel`),
  ADD KEY `id_sala` (`id_sala`);

--
-- Indexes for table `godziny_plany`
--
ALTER TABLE `godziny_plany`
  ADD KEY `id_godzina` (`id_godzina`),
  ADD KEY `id_plan` (`id_plan`);

--
-- Indexes for table `klasy`
--
ALTER TABLE `klasy`
  ADD UNIQUE KEY `nazwa` (`nazwa`),
  ADD UNIQUE KEY `id_klasa` (`id_klasa`),
  ADD KEY `id_kierunek` (`id_kierunek`),
  ADD KEY `id_nauczyciel` (`id_nauczyciel`);

--
-- Indexes for table `klasy_uczniowie`
--
ALTER TABLE `klasy_uczniowie`
  ADD KEY `id_uczen` (`id_uczen`),
  ADD KEY `id_klasa` (`id_klasa`);

--
-- Indexes for table `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD UNIQUE KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD UNIQUE KEY `kod` (`kod`),
  ADD KEY `id_miejscowosc` (`id_miejscowosc`);

--
-- Indexes for table `miejscowosci`
--
ALTER TABLE `miejscowosci`
  ADD UNIQUE KEY `id_miejscowosc` (`id_miejscowosc`),
  ADD UNIQUE KEY `nazwa` (`nazwa`);

--
-- Indexes for table `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD UNIQUE KEY `id_nauczyciel` (`id_nauczyciel`),
  ADD KEY `osoba` (`osoba`),
  ADD KEY `stopien` (`stopien`);

--
-- Indexes for table `osoby`
--
ALTER TABLE `osoby`
  ADD UNIQUE KEY `id_osoba` (`id_osoba`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `plany_zajec`
--
ALTER TABLE `plany_zajec`
  ADD UNIQUE KEY `id_plan` (`id_plan`),
  ADD KEY `id_klasa` (`id_klasa`);

--
-- Indexes for table `przedmioty`
--
ALTER TABLE `przedmioty`
  ADD UNIQUE KEY `id_przedmiot` (`id_przedmiot`);

--
-- Indexes for table `przedmioty_nauczyciele`
--
ALTER TABLE `przedmioty_nauczyciele`
  ADD KEY `id_przedmiot` (`id_przedmiot`),
  ADD KEY `id_nauczyciel` (`id_nauczyciel`);

--
-- Indexes for table `sale`
--
ALTER TABLE `sale`
  ADD UNIQUE KEY `id_sala` (`id_sala`),
  ADD KEY `id_szkola` (`id_szkola`);

--
-- Indexes for table `szkoly`
--
ALTER TABLE `szkoly`
  ADD UNIQUE KEY `id_szkola` (`id_szkola`),
  ADD KEY `id_kod_pocztowy` (`id_kod_pocztowy`),
  ADD KEY `id_ulica` (`id_ulica`);

--
-- Indexes for table `uczniowie`
--
ALTER TABLE `uczniowie`
  ADD UNIQUE KEY `uczen` (`uczen`),
  ADD KEY `id_osoba` (`id_osoba`),
  ADD KEY `forma_nauki` (`forma_nauki`),
  ADD KEY `kierunek` (`kierunek`);

--
-- Indexes for table `ulice`
--
ALTER TABLE `ulice`
  ADD UNIQUE KEY `id_ulica` (`id_ulica`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `godziny`
--
ALTER TABLE `godziny`
  MODIFY `id_godzina` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `klasy`
--
ALTER TABLE `klasy`
  MODIFY `id_klasa` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  MODIFY `id_kod_pocztowy` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `miejscowosci`
--
ALTER TABLE `miejscowosci`
  MODIFY `id_miejscowosc` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  MODIFY `id_nauczyciel` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `osoby`
--
ALTER TABLE `osoby`
  MODIFY `id_osoba` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `plany_zajec`
--
ALTER TABLE `plany_zajec`
  MODIFY `id_plan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `przedmioty`
--
ALTER TABLE `przedmioty`
  MODIFY `id_przedmiot` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `sale`
--
ALTER TABLE `sale`
  MODIFY `id_sala` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `szkoly`
--
ALTER TABLE `szkoly`
  MODIFY `id_szkola` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  MODIFY `uczen` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `ulice`
--
ALTER TABLE `ulice`
  MODIFY `id_ulica` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `godziny`
--
ALTER TABLE `godziny`
  ADD CONSTRAINT `godziny_ibfk_1` FOREIGN KEY (`id_nauczyciel`) REFERENCES `nauczyciele` (`id_nauczyciel`),
  ADD CONSTRAINT `godziny_ibfk_3` FOREIGN KEY (`id_sala`) REFERENCES `sale` (`id_sala`),
  ADD CONSTRAINT `godziny_ibfk_4` FOREIGN KEY (`id_przedmiot`) REFERENCES `przedmioty` (`id_przedmiot`);

--
-- Ograniczenia dla tabeli `godziny_plany`
--
ALTER TABLE `godziny_plany`
  ADD CONSTRAINT `godziny_plany_ibfk_1` FOREIGN KEY (`id_plan`) REFERENCES `plany_zajec` (`id_plan`),
  ADD CONSTRAINT `godziny_plany_ibfk_2` FOREIGN KEY (`id_godzina`) REFERENCES `godziny` (`id_godzina`);

--
-- Ograniczenia dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD CONSTRAINT `klasy_ibfk_1` FOREIGN KEY (`id_nauczyciel`) REFERENCES `nauczyciele` (`id_nauczyciel`);

--
-- Ograniczenia dla tabeli `klasy_uczniowie`
--
ALTER TABLE `klasy_uczniowie`
  ADD CONSTRAINT `klasy_uczniowie_ibfk_1` FOREIGN KEY (`id_uczen`) REFERENCES `uczniowie` (`uczen`),
  ADD CONSTRAINT `klasy_uczniowie_ibfk_2` FOREIGN KEY (`id_klasa`) REFERENCES `klasy` (`id_klasa`);

--
-- Ograniczenia dla tabeli `kody_pocztowe`
--
ALTER TABLE `kody_pocztowe`
  ADD CONSTRAINT `kody_pocztowe_ibfk_1` FOREIGN KEY (`id_miejscowosc`) REFERENCES `miejscowosci` (`id_miejscowosc`);

--
-- Ograniczenia dla tabeli `nauczyciele`
--
ALTER TABLE `nauczyciele`
  ADD CONSTRAINT `nauczyciele_osoby_fk` FOREIGN KEY (`osoba`) REFERENCES `osoby` (`id_osoba`);

--
-- Ograniczenia dla tabeli `osoby`
--
ALTER TABLE `osoby`
  ADD CONSTRAINT `osoby_ibfk_1` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`),
  ADD CONSTRAINT `osoby_ibfk_2` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`);

--
-- Ograniczenia dla tabeli `przedmioty_nauczyciele`
--
ALTER TABLE `przedmioty_nauczyciele`
  ADD CONSTRAINT `przedmioty_nauczyciele_ibfk_1` FOREIGN KEY (`id_przedmiot`) REFERENCES `przedmioty` (`id_przedmiot`),
  ADD CONSTRAINT `przedmioty_nauczyciele_ibfk_2` FOREIGN KEY (`id_nauczyciel`) REFERENCES `nauczyciele` (`id_nauczyciel`);

--
-- Ograniczenia dla tabeli `sale`
--
ALTER TABLE `sale`
  ADD CONSTRAINT `sale_ibfk_1` FOREIGN KEY (`id_szkola`) REFERENCES `szkoly` (`id_szkola`);

--
-- Ograniczenia dla tabeli `szkoly`
--
ALTER TABLE `szkoly`
  ADD CONSTRAINT `szkoly_ibfk_1` FOREIGN KEY (`id_kod_pocztowy`) REFERENCES `kody_pocztowe` (`id_kod_pocztowy`),
  ADD CONSTRAINT `szkoly_ibfk_2` FOREIGN KEY (`id_ulica`) REFERENCES `ulice` (`id_ulica`);

--
-- Ograniczenia dla tabeli `uczniowie`
--
ALTER TABLE `uczniowie`
  ADD CONSTRAINT `uczniowie_ibfk_1` FOREIGN KEY (`id_osoba`) REFERENCES `osoby` (`id_osoba`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
